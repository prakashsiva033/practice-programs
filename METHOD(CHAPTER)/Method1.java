package Methods;

class Person{
	private String name;
	private int age;
	Person(){
		name="Sivaa";
		age=20;
	}

	void talking() {
		System.out.println("Name is " + name);
		System.out.println("Age is " + age);

	}

}

public class Method1 {
	public static void main(String[] args) {
		Person raja=new Person();
		//raja.name="rajan";
		//raja.age=24;
		//System.out.println("Hashcode is :"+raja.hashCode());
		raja.talking(); 
		Person jak=new Person();
		jak.talking();
	}
	
}
