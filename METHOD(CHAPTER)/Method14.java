package Methods;

class Person14 {
	int id;
	Person14(int x){
		id=x;
	}

}

class check{
	void swap(Person14 p1,Person14 p2) {
		Person14 temp=p1;
		p1=p2;
		p2=temp;
	}
	
}

public class Method14 {

	public static void main(String[] args) {
		Person14 p1=new Person14(10);
		Person14 p2=new Person14(11);
		System.out.println("before calling "+p1.id);
		System.out.println("before calling "+p2.id);
		check c=new check();
		c.swap(p1,p2);
		System.out.println("after calling "+p1.id);
		System.out.println("after calling "+p2.id);
		
		
		

	}

}
