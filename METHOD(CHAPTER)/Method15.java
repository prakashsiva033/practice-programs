package Methods;
class Person15{
	int id1;
	int id2;
	Person15(int id1,int id2){
		this.id1=id1;
		this.id2=id2;
	}
}
class checkk{
	void swap(Person15 p) {
		int temp;
		temp=p.id1;
		p.id1=p.id2;
		p.id2=temp;
		
		
	}
}
public class Method15 {
	public static void main(String[] args) {
		Person15 p=new Person15(1,2);
		System.out.println("before"+p.id1+"\t"+p.id2);
		checkk c=new checkk();
		c.swap(p);
		System.out.println("after"+p.id1+"\t"+p.id2);
	}

}
