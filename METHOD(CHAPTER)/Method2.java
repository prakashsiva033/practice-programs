package Methods;

class Person1{
	private String name;
	private int age;
	Person1(){
		name="siva";
		age=19;
	}
	Person1(int i,String s){
		name=s;
		age=i;
	}
	void talking() {
		System.out.println("the name is : "+name);
		System.out.println("the age is : "+age);
	}
	
	
}

public class Method2 {
	public static void main(String[] args) {
		Person1 siva=new Person1();
		siva.talking();
		Person1 raja=new Person1(33,"Raja");
		raja.talking();
		
		
	}

}
