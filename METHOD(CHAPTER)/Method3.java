package Methods;

import java.io.*;

class Person3 {
	private String name;
	private int age;

	void accept() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter your name and age : ");
		name = br.readLine();
		age = Integer.parseInt(br.readLine());
	}

	void check() {
		if (age <= 30) {
			System.out.println("you" + name + "is average age");
		} else if (age <= 50) {
			System.out.println("you" + name + "is middle age");
		} else {
			System.out.println("you are expired");
		}
	}
}

public class Method3 {
	public static void main(String[] args) throws IOException {
		Person3 gangi = new Person3();
		gangi.accept();
		gangi.check();

	}

}
