package Methods;
class Person5{
	private int a;
	private int b;
	Person5(int i,int c){
		a=i;
		b=c;
	}
	int add() {
		int c=a+b;
		System.out.println(c);
		return c;
	}
}
public class Method5 {
	public static void main(String[] args) {
		Person5 p=new Person5(10,10);
		int d=p.add();
		System.out.println(d);
	}

}
