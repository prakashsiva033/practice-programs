package Methods;
class Person13{
	void swap(int x,int y) {
		int temp=x;
		x=y;
		y=temp;
		System.out.println("in method x "+x);
		System.out.println("in method y "+y);
	}
}
public class Methods13 {
       public static void main(String[] args) {
    	//   int x=10;
    	 //  int y=20;
    	   Person13 p=new Person13();
    	   p.swap(10, 20);
    	 //  System.out.println("in main x "+x);
    	   //System.out.println("in main y "+y);
    	   
       }
}
