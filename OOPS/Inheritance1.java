package oopsconcept;
//teacher
class Inheritance {
	private int id;
	private String name;
	private String address;
	private float salary;
	public void setid(int id) {
		this.id=id;
	}
	int getid() {
		return id;
	}
	void setname(String name) {
		this.name=name;
	}
	String getname() {
		return name;
	}
    void setaddress(String address) {
    	this.address=address;
    }
    String getaddress() {
    	return address;
    }
    void setsalary(float salary) {
    	this.salary=salary;
    }
    float getsalary() {
    	return salary;
    }
}
class Student extends Inheritance{ 
	int marks;
	void setMarks(int marks) {
		this.marks=marks;
	}
	int getmarks() {
		return marks;
	}
}
class Inheritance1 {
	public static void main(String args[]) {
		Student s=new Student();
		s.setMarks(11);
		s.setname("anil");;
		Inheritance i=new Inheritance();
		i.setname("siva");
		System.out.println("stud class :"+s.getmarks());
		System.out.println("stud class : "+s.getname());
		System.out.println("Inheri class :"+i.getname());
		
	}
}