package oopsconcept;

class first{
	int i=10;
	void show() {
		System.out.println("first class variable is : "+i );
	}
}
class second extends first{
	private int i=20;
	void show() {
		
		System.out.println("Second class variable is : "+i);
		super.show();
		System.out.println("just : "+super.i);
	}
}

public class Inheritance2 {
	public static void main(String[] args) {
		second s=new second();
		s.show();
	}

}
