package oopsconcept;
class first4{
	int i;
	first4(int i){
		this.i=i;
	}
}
class second4 extends first4{
	int i;
	second4(int i,int j){
		super(j);
		this.i=i;
		
	}
	void show() {
		System.out.println("sub class : "+i);
		System.out.println("super class : "+super.i);
	}
}
public class Inheritance4 {
	public static void main(String[] args) {
		second4 s=new second4(10,22);
		s.show();
	}

}
