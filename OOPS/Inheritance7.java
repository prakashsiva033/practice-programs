package oopsconcept;

class shape {
	protected int len;

	shape(int len) {
		this.len = len;
	}
}
	class square extends shape {

		square(int length) {
			super(length);
		}

		void square() {
			System.out.println("the square value is : " + len * len);
		}

	}

	class rectangle extends square {
		int bre;

		rectangle(int le, int re) {
			super(le);
			bre = re;
		}

		void result() {
			System.out.println("the rectangle values is : " + len * bre);
		}
	}

public class Inheritance7{

public static void main(String[] args) {
//square s=new square(5);
//s.square();
	rectangle r=new rectangle(3,2);
	r.result();
	r.square();
	r.square();
}
}
