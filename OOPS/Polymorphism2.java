package oopsconcept;
class overriding1{
	void calculate(double x)
	{
		System.out.println(x*x);
	}
}
class overriding2 extends overriding1{
	void calculate(double x) {
		System.out.println(Math.sqrt(x));
	}
}

public class Polymorphism2 {

	public static void main(String[] args) {
		overriding1 o=new overriding1();
		o.calculate(5);
		

	}

}
