package oopsconcept;
class staticmethod{
	static void add(int x) {
		System.out.println(x*x);
		
	}
}
class staticmethod2 extends staticmethod{
	static void add(int x) {
		System.out.println(Math.sqrt(x));
	}
}

public class Polymorphism3 {
	public static void main(String[] args) {
		staticmethod2 s=new staticmethod2();
		s.add(2);
	}

}
