package oopsconcept;
class commercial{
	String name;
	void setName(String name) {
		this.name=name;
	}
	String getName() {
		return name;
	}
	void calculates(int units) {
		System.out.println(getName());
		System.out.println(units*5);
	}
}
class Domestric extends commercial{
	void calculates(int units) {
		System.out.println(getName());
		System.out.println(units*2.50);
	}
}

public class Polymorphism4 {
	public static void main(String[] args) {
		commercial c=new commercial();
		c.setName("siva");
		c.calculates(100);
		Domestric d=new Domestric();
		d.setName("raja");
		d.calculates(100);
	}

}
