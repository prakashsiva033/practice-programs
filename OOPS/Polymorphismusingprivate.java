package oopsconcept;
class staticmethod3{
	private void add(int x) {
		System.out.println(x*x);
		
	}
}
class staticmethod4 extends staticmethod3{
	private void add(int x) {
		System.out.println(Math.sqrt(x));
	}
	public void access() {
		staticmethod2 s=new staticmethod2();
		s.add(25);
	}
}

public class Polymorphismusingprivate {
	public static void main(String[] args) {
		staticmethod4 s=new staticmethod4();
		s.access();
	}

}
